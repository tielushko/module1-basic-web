window.onload = confirmUser()

//function to confirm the user exists from the server 
//if exists, use get() to get the current score, else post() to create a new user with score 0
function confirmUser() {
  window.URL = "http://basic-web.dev.avc.web.usf.edu/"
  
  //get the user's name
  const searchParameter = window.location.search
  const usernameString = new URLSearchParams(searchParameter);
  const username = (usernameString.get('id'))
  
  window.URL = window.URL + username;
  window.counter = 0;

  get(window.URL).then(function(response) {
    if (response.status == 200) {
      const id = response.data.id;
      window.score = response.data.score;
      document.getElementById("userWelcome").innerHTML = id;
      let outputValue 
      
      //fizzbuzz to display the user's score in correct form once requesting data from server
      if (window.score == 0) {
        outputValue = 0
      } else if (window.score % 3 == 0) {
        outputValue = "Fizz"
      } else if (window.score % 5 == 0) {
        outputValue = "Buzz"
      } else if (window.score % 15 == 0) {
        outputValue = "FizzBuzz"
      } else {
        outputValue = window.score;
      }
      document.getElementById("userFizzBuzz").innerHTML = outputValue;
    } else {
      post(window.URL, {score: 0});
      window.score = 0;
      document.getElementById("userWelcome").innerHTML = username;
      document.getElementById("userFizzBuzz").innerHTML = window.score;
    }
  });
}

//GET function to read the username from the server and retrieve
function get(url) {
  return new Promise((resolve, reject) => {
    const http = new XMLHttpRequest();
    http.onload = function() {
      resolve({ status: http.status, data: JSON.parse(http.response) });
    };
    http.open("GET", url);
    http.send();
  });
}

//POST function to create a new user if it doesn't exist and set the score to 0
function post(url, data) {
  data = JSON.stringify(data);
  return new Promise((resolve, reject) => {
    const http = new XMLHttpRequest();
    http.onload = function() {
      resolve({ status: http.status, data: JSON.parse(http.response) });
    };
    //dealing with server errors.
    http.ontimeout = function () {
      document.getElementById("userFizzBuzz").innerHTML = "Timeout of server: took too long to respond";
      document.getElementsByClassName("incrementor").innerHTML.disabled = true;
    }
    http.onerror = function () {
      document.getElementById("userFizzBuzz").innerHTML = "Issues reaching the server";
      document.getElementsByClassName("incrementor").innerHTML.disabled = true;
    }
    http.open("POST", url);
    //Make sure that the server knows we're sending it json data.
    http.setRequestHeader("Content-Type", "application/json");
    http.timeout = 500;
    http.send(data);
    console.log(http.status);
  });
}

//play function that is started from the login screen window
function play() {
    if (window.counter == 0) {
      window.counter++;
      game(window.score);
    } else {
      game(window.storedScore);
    }
  }

function game(currentScore) {
    currentScore++ // Uptdates current score
    window.storedScore = currentScore
    const dataToSend = { score: currentScore };
    post(window.URL, dataToSend).then(function (response) {
      switch (response.status) {
        case 200:
          window.score = response.data.score;
          break;
        case 201:
          window.score = response.data.score;
          break;
        case 400:
          response.data = "Error: Invalid Request!"
          console.error(response.data);
          break;
        case 500:
          response.data = "Error: Internal Server Error!"
          console.error(response.data);
          break;
        }
    });
      let outputValue
      // The conditional staments checks the current score to display in the main page the correct score (integer, Fizz, Buzz, or FizzBuzz)
      if (currentScore % 15 == 0) {
        outputValue = 'FizzBuzz'
      } else if (currentScore % 3 == 0) {
        outputValue = 'Fizz'
      } else if (currentScore % 5 == 0) {
        outputValue = 'Buzz'
      }
      else {
        outputValue = currentScore;
      }
      document.getElementById("userFizzBuzz").innerHTML = outputValue;
  }
